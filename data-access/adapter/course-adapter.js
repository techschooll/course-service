const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class CourseAdapter extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'courses'
    this.connectionstring = _.get(this, `requestContext.config.db.connectionstring`,"")+ this.tableName
  }

  async insertCourse(payload) {
    try {
      let response = await this.post(this.connectionstring, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async updateCourse(payload) {
    try {
      let response = await this.put(this.connectionstring + "/" + payload.id, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getCourse(Id) {
    try {
      let response = await this.get(this.connectionstring + "/" + Id)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getAll() {
    try {
      let response = await this.get(this.connectionstring)
      return response.data
    }
    catch(e) {
      throw e
    }
  }
}

module.exports = CourseAdapter