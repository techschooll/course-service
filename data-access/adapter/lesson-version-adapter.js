const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class LessonVersion extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'lesson_version'
    this.connectionstring = _.get(this, `requestContext.config.db.connectionstring`,"")+ this.tableName
  }

  async getVersionByLessonAndUser(lessonId, userId) {
    try {
      let response = await this.get(this.connectionstring + "?lesson_id=" + lessonId + "&author=" + userId)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getVersionById(Id) {
    try {
      let response = await this.get(this.connectionstring + "/" + Id)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async insertLessonVersion(payload) {
    try {
      let response = await this.post(this.connectionstring, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async updateLessonVersion(payload) {
    try {
      let response = await this.put(this.connectionstring + "/" + payload.id, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async removeLessonVersion(Id) {
    try {
      let response = await this.delete(this.connectionstring + "/" + Id)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getAllVersionForLesson(lessonId) {
  try {
      let response = await this.get(this.connectionstring + "/?lesson_id=" + lessonId)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

}

module.exports = LessonVersion