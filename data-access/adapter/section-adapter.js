const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class SectionAdapter extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'sections'
    this.connectionstring = _.get(this, `requestContext.config.db.connectionstring`,"")+ this.tableName
  }

  async insertSection(payload) {
    try {
      let response = await this.post(this.connectionstring, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async updateSection(payload) {
    try {
      let response = await this.put(this.connectionstring + "/" + payload.id, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getSection(Id) {
    try {
      let response = await this.get(this.connectionstring + "/" + Id)
      return response.data
    }
    catch(e) {
      throw e
    }
  }
}

module.exports = SectionAdapter