const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class LessonAdapter extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'lessons'
    this.connectionstring = _.get(this, `requestContext.config.db.connectionstring`,"")+ this.tableName
  }

  async insertLesson(payload) {
    try {
      let response = await this.post(this.connectionstring, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async updateLesson(payload) {
    try {
      let response = await this.put(this.connectionstring + "/" + payload.id, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getLesson(id) {
    try {
      let response = await this.get(this.connectionstring + "/" + id)
      return response.data
    }
    catch(e) {
      throw e
    }
  }
}

module.exports = LessonAdapter