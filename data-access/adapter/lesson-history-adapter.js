const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class LessonHistory extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'lesson_history'
    this.connectionstring = _.get(this, `requestContext.config.db.connectionstring`,"")+ this.tableName
  }

  async insertLessonHistory(payload) {
    try {
      let response = await this.post(this.connectionstring, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async getLessonHistory(lessonId, version) {
    try {
      let response = await this.get(this.connectionstring + "?lesson_id=" + lessonId + "&version=" + version)
      return response.data
    }
    catch(e) {
      throw e
    }
  }
}

module.exports = LessonHistory