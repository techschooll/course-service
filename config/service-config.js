let config = {
  "server": {
    "port": 3600,
    "host": "localhost"
  },
  "service": "course-service",
  "db": {
    "connectionstring": "http://localhost:4000/"
  },
  "routes": {
    "auth": {
      "/v1/course/create-lesson": ["AUT", "ADM"],
      "/v1/course/fork-lesson/{id}": ["AUT", "ADM"],
      "/v1/course/update-lesson": ["AUT", "ADM"],
      "/v1/course/merge-lesson/{id}": ["AUT", "ADM"],
      "/v1/course/update-lesson-status": ["AUT", "ADM"],
      "/v1/course/resolve-conflict": ["AUT", "ADM"],
      "/v1/course/create-section": ["AUT", "ADM"],
      "/v1/course/update-section": ["AUT", "ADM"],
      "/v1/course/update-course": ["AUT", "ADM"]
    }
  },
  "jwt": {
    "secret": "c4929650-9241-4f46-9643-6297e686d8f7"
  }
}

module.exports = config