const Config = require('../../config/service-config')
const Uuid = require('uuid')
const _ = require('lodash')
const LessonAdapter = require('../../data-access/adapter/lesson-adapter')
const LessonVersionAdapter = require('../../data-access/adapter/lesson-version-adapter')
const LessonHistoryAdapter = require('../../data-access/adapter/lesson-history-adapter')
const SectionAdapter = require('../../data-access/adapter/section-adapter')
const CourseAdapter = require('../../data-access/adapter/course-adapter')
const Enum = require('../common/enum')

class CourseService {
  constructor(requestContext, credentials) {
    if (requestContext) requestContext = JSON.parse(requestContext)
    if (credentials) credentials = JSON.parse(credentials)
    requestContext.config = Config
    requestContext.credentials = credentials
    this.requestContext = requestContext
    this.lessonAdapter = new LessonAdapter(this.requestContext)
    this.lessonVersionAdapter = new LessonVersionAdapter(this.requestContext)
    this.lessonHistoryAdapter = new LessonHistoryAdapter(this.requestContext)
    this.sectionAdapter = new SectionAdapter(this.requestContext)
    this.courseAdapter = new CourseAdapter(this.requestContext)
  }

  //private functions go here.. 

  async _asyncForEach(array = [], callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  async _checkIfVersionExistsForUser(lessonId, userId) {
    try {
      let response = this.lessonVersionAdapter.getVersionByLessonAndUser(lessonId, userId)
      return response
    } catch (e) {
      //gracefully handling error to create a new copy of the lesson if it doesnt exist
    }
  }

  async _mergeVersions(userVersion, masterVersion) {
    try {
      let lessonPayload = {
        "status": "merged",
        "meta": _.get(userVersion, `data.meta`),
        "content": _.get(userVersion, `data.content`),
        "version": masterVersion.version + 1,
        "id": masterVersion.id
      }
      let lessonHistory = {
        "version": masterVersion.version,
        "lesson_id": masterVersion.lesson_id,
        "snapshot": masterVersion
      }
      await this.lessonVersionAdapter.removeLessonVersion(userVersion.id)
      await this.lessonHistoryAdapter.insertLessonHistory(lessonHistory)
      let response = await this.lessonAdapter.updateLesson(lessonPayload)
      return response
    } catch (e) {
      throw e
    }
  }

  _getObjectDiff(obj1, obj2) {
    const diff = Object.keys(obj1).reduce((result, key) => {
      if (!obj2.hasOwnProperty(key)) {
        result.push(key);
      } else if (_.isEqual(obj1[key], obj2[key])) {
        const resultKeyIndex = result.indexOf(key);
        result.splice(resultKeyIndex, 1);
      }
      return result;
    }, Object.keys(obj2));

    return diff;
  }

  async _validateStateTransition(currentStatus, status, lessonId, userId) {
    try {
      if (Enum.lessonStates.indexOf(status) - Enum.lessonStates.indexOf(currentStatus) != 1) {
        return {
          status: 409,
          error: 'InvalidStateTransition'
        }
      }
      let draftVersionLength = await this.lessonVersionAdapter.getVersionByLessonAndUser(lessonId, userId)
      if (status == 'saved' && await draftVersionLength.length != 0) {
        return {
          status: 409,
          error: 'InvalidStateTransition',
          message: 'Please merge draft version before saving'
        }
      }
      return {
        status: 200
      }
    } catch (e) {
      throw e
    }
  }

  async _validateSectionPayload(payload) {
    try {
      let isValid = true
      if (_.get(payload, `content.lesson_sequence`, []).length > 0) {
        await this._asyncForEach(payload.content.lesson_sequence, async (seq) => {
          let lesson = await this.lessonAdapter.getLesson(seq)
          if (!lesson || lesson.status != 'published') {
            isValid = false
          }
        })
      }
      if (!isValid) {
        return {
          status: 409,
          error: 'InvalidLessonInSection'
        }
      }
      return {
        status: 200
      }
    } catch (e) {
      throw e
    }
  }

  async _validateCoursePayload(payload) {
    try {
      let isValid = true
      if (_.get(payload, `content.section_sequence`, []).length > 0) {
        await this._asyncForEach(payload.content.section_sequence, async (seq) => {
          let section = await this.sectionAdapter.getSection(seq)
          if (!section) {
            isValid = false
          }
        })
      }
      if (!isValid) {
        return {
          status: 409,
          error: 'InvalidSectionInCourse'
        }
      }
      return {
        status: 200
      }
    } catch (e) {
      throw e
    }

  }

  //public methods go here.. 

  async createLesson(payload) {
    try {
      _.set(payload, 'status', 'created')
      _.set(payload, 'version', 1)
      let response = await this.lessonAdapter.insertLesson(payload)
      console.log(response)
      return response
    } catch (e) {
      throw e
    }
  }

  async forkLesson(payload) {
    try {
      let existingLesson = await this._checkIfVersionExistsForUser(payload.id, _.get(this, `requestContext.credentials.data.user_id`))
      if (existingLesson.length > 0) {
        return existingLesson[0]
      }
      let lesson = await this.lessonAdapter.getLesson(payload.id)
      if (lesson.status != 'created' && lesson.status != 'merged') {
        throw {
          status: 409,
          error: "InvalidFork"
        }
      }
      let lessonVersionPayload = {
        "base_version": _.get(lesson, 'version'),
        "lesson_id": _.get(lesson, 'id'),
        "author": _.get(this, `requestContext.credentials.data.user_id`),
        "status": "draft",
        "data": {
          meta: lesson.meta,
          content: lesson.content
        }
      }
      let lessonVersion = await this.lessonVersionAdapter.insertLessonVersion(lessonVersionPayload)
      return lessonVersion
    } catch (e) {
      throw e
    }
  }

  async updateLesson(payload) {
    try {
      let existingVersion = await this.lessonVersionAdapter.getVersionById(payload.id)
      _.set(payload, `lesson_id`, existingVersion.lesson_id)
      _.set(payload, `base_version`, existingVersion.base_version)
      _.set(payload, `author`, existingVersion.author)
      _.set(payload, `status`, existingVersion.status)
      let lessonVersion = await this.lessonVersionAdapter.updateLessonVersion(payload)
      return lessonVersion
    } catch (e) {
      throw e
    }
  }

  async mergeLesson(payload) {
    try {
      let userVersion = await this._checkIfVersionExistsForUser(payload.id, _.get(this, `requestContext.credentials.data.user_id`))
      if (!userVersion) {
        return {
          status: 409,
          error: "InvalidMergeRequest"
        }
      }
      userVersion = userVersion[0]
      let masterVersion = await this.lessonAdapter.getLesson(userVersion.lesson_id)
      //when user has forked from master branch
      if (masterVersion.version == userVersion.base_version) {
        let response = await this._mergeVersions(userVersion, masterVersion)
        return response
      } else {
        let baseVersion = await this.lessonHistoryAdapter.getLessonHistory(userVersion.lesson_id, userVersion.base_version)
        baseVersion = baseVersion[0].snapshot
        if (_.isEqual(baseVersion.meta, masterVersion.meta) && _.isEqual(baseVersion.content, masterVersion.content)) {
          await this._mergeVersions(userVersion, masterVersion)
        } else {
          let diff = this._getObjectDiff(baseVersion.meta, masterVersion.meta) +
            this._getObjectDiff(baseVersion.content, masterVersion.content)
          return {
            status: 409,
            error: "Please resolve the conflicts in the following keys " + JSON.stringify(diff)
          }
        }
      }
    } catch (e) {
      throw e
    }
  }

  async updateLessonAndMerge(payload) {
    try {
      await this.updateLesson(payload)
      let userVersion = await this.lessonVersionAdapter.getVersionById(payload.id)
      let masterVersion = await this.lessonAdapter.getLesson(userVersion.lesson_id)
      let response = await this._mergeVersions(userVersion, masterVersion)
      return response
    } catch (e) {
      throw e
    }
  }

  async updateLessonStatus(payload) {
    try {
      let lesson = await this.lessonAdapter.getLesson(payload.id)
      let isValid = await this._validateStateTransition(lesson.status, payload.status, payload.id, _.get(this, `requestContext.credentials.data.user_id`))
      if (isValid.status != 200) {
        return isValid
      }
      let lessonPayload = {
        "status": payload.status,
        "meta": _.get(lesson, `meta`),
        "content": _.get(lesson, `content`),
        "version": lesson.version,
        "id": lesson.id
      }
      let response = await this.lessonAdapter.updateLesson(lessonPayload)
      return response
    } catch (e) {
      throw e
    }
  }

  async createSection(payload) {
    try {
      let isValid = await this._validateSectionPayload(payload)
      if (isValid.status != 200) {
        return isValid
      }
      let response = await this.sectionAdapter.insertSection(payload)
      return response
    } catch (e) {
      throw e
    }
  }

  async updateSection(payload) {
    try {
      let isValid = await this._validateSectionPayload(payload)
      if (isValid.status != 200) {
        return isValid
      }
      let response = await this.sectionAdapter.updateSection(payload)
      return response
    } catch (e) {
      throw e
    }
  }

  async insertOrUpdateCourse(payload) {
    try {
      let isValid = await this._validateCoursePayload(payload)
      if (isValid.status != 200) {
        return isValid
      }
      let response
      if (payload.id) {
        response = await this.courseAdapter.updateCourse(payload)
      } else {
        response = await this.courseAdapter.insertCourse(payload)
      }
      return response
    } catch (e) {
      throw e
    }
  }

  async fetchAllCourses() {
    let courses = await this.courseAdapter.getAll()
    courses = _.filter(courses, (course) => {
      return course.status == "published"
    })
    await this._asyncForEach(courses, async(course) => {
      course.content = course.content || {}
      _.set(course, `content.sections`, [])
      await this._asyncForEach(course.content.section_sequence, async(section) => {
        let sectionDetails = await this.sectionAdapter.getSection(section)
        sectionDetails.content = sectionDetails.content || {}
        _.set(sectionDetails, `content.lessons`, [])
        await this._asyncForEach(sectionDetails.content.lesson_sequence, async(lesson) => {
          let lessonDetails = await this.lessonAdapter.getLesson(lesson) 
          sectionDetails.content.lessons.push(lessonDetails)
        })
        course.content.sections.push(sectionDetails)
      })
    })
    return courses
  }
}


module.exports = CourseService;