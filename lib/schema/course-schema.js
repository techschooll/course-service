const joi = require('@hapi/joi')

const lessonBaseSchema = joi.object({
  meta: joi.object().optional(),
  content: joi.object().optional()
})

const lessonResponseSchema = lessonBaseSchema.keys({
  id: joi.number().required(),
  status: joi.string().optional(),
  version: joi.number().optional()
})

const updateLessonVersionSchema = joi.object({
  id: joi.number().required(),
  data: joi.object().optional()
})


const localExports = {
  lessonBaseSchema,
  lessonResponseSchema,
  updateLessonVersionSchema
}

module.exports = localExports