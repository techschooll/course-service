const lessonStates = ['created', 'draft', 'merged', 'saved', 'published']

const localExports = {
  lessonStates
}
module.exports = localExports 