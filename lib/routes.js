const CourseService = require('./service/course-service')
const Schema = require('../lib/schema/course-schema')
const BaseHelper = require('base-lib').Helper
const Config = require('../config/service-config')

const authenticate = async (request, h) => {
  try {
    let credentials = await new BaseHelper({
      Config
    }).authenticateRequest(request.headers["x-jwt"], request, Config, Config.routes.auth[request._route.path])
    return true
  } catch (e) {
    throw e
  }
}

const routes = [{
  method: 'POST',
  path: '/v1/course/create-lesson',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }],
    validate: {
      payload: Schema.lessonBaseSchema
    },
    response: {
      schema: Schema.lessonResponseSchema,
    }
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.createLesson(req.payload)
  }
},{
  method: 'GET',
  path: '/v1/course/fork-lesson/{id}',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.forkLesson(req.params)
  }
},{
  method: 'POST',
  path: '/v1/course/update-lesson',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }],
    validate: {
      payload: Schema.updateLessonVersionSchema
    }
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.updateLesson(req.payload)
  }
},{
  method: 'POST',
  path: '/v1/course/resolve-conflict',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }],
    validate: {
      payload: Schema.updateLessonVersionSchema
    }
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.updateLessonAndMerge(req.payload)
  }
},{
  method: 'POST',
  path: '/v1/course/merge-lesson/{id}',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.mergeLesson(req.params)
  }
},{
  method: 'POST',
  path: '/v1/course/update-lesson-status',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.updateLessonStatus(req.payload)
  }
},{
  method: 'POST',
  path: '/v1/course/create-section',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.createSection(req.payload)
  }
},{
  method: 'POST',
  path: '/v1/course/update-section',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.updateSection(req.payload)
  }
},{
  method: 'POST',
  path: '/v1/course/update-course',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.insertOrUpdateCourse(req.payload)
  }
},
{
  method: 'GET',
  path: '/v1/course/fetch-all-courses',
  config: {
    tags: ['api']
  },
  handler: async function(req, h) {
    let courseServiceObj = new CourseService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await courseServiceObj.fetchAllCourses()
  }
}]

module.exports = routes;