const assert = require('chai').assert;
const CourseService = require('../lib/service/course-service')
const Config = require('../config/service-config')
const uuid = require('uuid')
const _ = require('lodash')

describe('User Service Tests', async function() {
    let requestContext = JSON.stringify({
        config: Config
    })
    let courseServiceObj = new CourseService(requestContext)
    let lessonId, author1Version, author2Version, sectionId
    it('Author is able to create a new lesson', async function() {
        try {
            let response = await courseServiceObj.createLesson({
                "meta": {
                    "name": "Test Course",
                    "length": "02:00:50",
                    "type": "video"
                },
                "content": [{
                    "link": "link goes here",
                    "description": "Test Description"
                }]
            })
            lessonId = response.id
            assert.equal(response.meta.name, "Test Course")
        } catch (e) {
            throw e
        }
    })
    it('author 1 forks a lesson', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.forkLesson({
                id: lessonId
            })
            author1Version = response.id
            assert.equal(response.author, "author1@abc.com")
            assert.equal(response.status, "draft")
        } catch (e) {
            throw e
        }
    })

    it('author 2 forks a lesson', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author2@abc.com")
            let response = await courseServiceObj.forkLesson({
                id: lessonId
            })
            author2Version = response.id
            assert.equal(response.author, "author2@abc.com")
            assert.equal(response.status, "draft")
        } catch (e) {
            throw e
        }
    })

    it('author 1 forks the same lesson and existing version is returned', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.forkLesson({
                id: lessonId
            })
            assert.equal(response.id, author1Version)
        } catch (e) {
            throw e
        }
    })

    it('author 2 updates the lesson contents', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author2@abc.com")
            let response = await courseServiceObj.updateLesson({
                "data": {
                    "meta": {
                        "name": "author 2 name",
                        "length": "02:00:50",
                        "type": "video"
                    },
                    "content": []
                },
                "id": author2Version
            })
            assert.equal(response.data.meta.name, "author 2 name")
        } catch (e) {
            throw e
        }
    })

    it('author 2 merges his version', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author2@abc.com")
            let response = await courseServiceObj.mergeLesson({
                "id": lessonId
            })
            assert.equal(response.meta.name, "author 2 name")
        } catch (e) {
            throw e
        }
    })

    it('author 1 updates the lesson contents', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.updateLesson({
                "data": {
                    "meta": {
                        "name": "author 1 name",
                        "length": "02:00:50",
                        "type": "video"
                    },
                    "content": []
                },
                "id": author1Version
            })
            assert.equal(response.data.meta.name, "author 1 name")
        } catch (e) {
            throw e
        }
    })

    it('author 1 tries to merges his version and conflict is thrown', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.mergeLesson({
                "id": lessonId
            })
            assert.equal(response.error.indexOf("Please resolve the conflicts in the following keys"), 0)
        } catch (e) {
            throw e
        }
    })

    it('author 1 resolves the conflict and merges his version', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.updateLessonAndMerge({
                "data": {
                    "meta": {
                        "name": "Final Title",
                        "length": "02:00:50",
                        "type": "video"
                    },
                    "content": []
                },
                "id": author1Version
            })
            assert.equal(response.status, "merged")
            assert.equal(response.meta.name, "Final Title")
            let versionHistory = await courseServiceObj.lessonVersionAdapter.getAllVersionForLesson(lessonId)
            //since all version are merged, remaining version length should be 0
            assert.equal(versionHistory.length, 0)
        } catch (e) {
            throw e
        }
    })

    it('author 1 tries to publish the lesson without saving', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.updateLessonStatus({
                "id": lessonId,
                "status": "published"
            })
            assert.equal(response.error, "InvalidStateTransition")
        } catch (e) {
            throw e
        }
    })

    it('author 1 creates a new section and tries to add an unpublished lesson to the section', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.createSection({
                "meta": {
                    "name": "new section",
                    "description": "Description goes here"
                },
                "content": {
                    "lesson_sequence": [lessonId]
                },
                "id": 2
            })
            assert.equal(response.error, "InvalidLessonInSection")
        } catch (e) {
            throw e
        }
    })

    it('author 1 tries save and then publish the lesson', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.updateLessonStatus({
                "id": lessonId,
                "status": "saved"
            })
            assert.equal(response.status, "saved")
            response = await courseServiceObj.updateLessonStatus({
                "id": lessonId,
                "status": "published"
            })
            assert.equal(response.status, "published")
        } catch (e) {
            throw e
        }
    })

    it('author 1 creates a new section and tries to add a published lesson to the section', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.createSection({
                "meta": {
                    "name": "new section",
                    "description": "Description goes here"
                },
                "content": {
                    "lesson_sequence": [lessonId]
                }
            })
            sectionId = response.id
            assert.equal(response.content.lesson_sequence.length, 1)
        } catch (e) {
            throw e
        }
    })

    it('author 2 updates section name', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author2@abc.com")
            let response = await courseServiceObj.updateSection({
                "meta": {
                    "name": "author 2 section",
                    "description": "Description goes here"
                },
                "content": {
                    "lesson_sequence": [lessonId]
                },
                "id": sectionId
            })
            assert.equal(response.meta.name, "author 2 section")
            assert.equal(response.content.lesson_sequence[0], lessonId)
        } catch (e) {
            throw e
        }
    })

    it('author 2 creates a new published course and adds a valid', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author2@abc.com")
            let response = await courseServiceObj.insertOrUpdateCourse({
                "meta": {
                    "name": "Course on DeepLearning",
                    "description": "Extensive course on DeepLearning"
                },
                "status": "published",
                "content": {
                    "section_sequence": [
                        sectionId
                    ]
                }
            })
            assert.equal(response.meta.name, "Course on DeepLearning")
            assert.equal(response.content.section_sequence[0], sectionId)
        } catch (e) {
            throw e
        }
    })

    it('author 1 creates a new un-published course', async function() {
        try {
            _.set(courseServiceObj, `requestContext.credentials.data.user_id`, "author1@abc.com")
            let response = await courseServiceObj.insertOrUpdateCourse({
                "meta": {
                    "name": "New Course",
                    "description": ""
                },
                "status": "created",
                "content": {
                }
            })
            assert.equal(response.meta.name, "New Course")
        } catch (e) {
            throw e
        }
    })

    it('user clicks on fetch all courses', async function() {
        try {
            let response = await courseServiceObj.fetchAllCourses()
            //ensure only published courses are fetched
            let count = 0
            _.forEach(response, (course) => {
                if(course.status == "published") ++count
            })
            assert.equal(response.length, count)
        } catch (e) {
            throw e
        }
    })

})